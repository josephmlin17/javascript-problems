/**
 * This returns a function, which returns a function, which returns a sum of all of the parameters.
 * It goes (x) + (y) + (z)
 * @param {number} x number to add
 * @return a Function that returns a Function which returns the sum of all of the parameters 
 */
export const add = (x) => {
	if (typeof x !== 'number') {
		throw 'Invalid input';
	}
	let sum = x; // closure variable here which is in scope of the other two functions

	/**
	 * @param {number} z 3rd number to add
	 * @returns the sum
	 */
	const plus3 = (z) => {
		if (typeof z !== 'number') {
			throw 'Invalid input';
		}
		return sum + z;
	}

	/**
	 * @param {number} y 2nd number to add
	 * r@returns a Function that returns a sum
	 */
	const plus2 = (y) => {
		if (typeof y !== 'number') {
			throw 'Invalid input';
		}
		sum += y;
		return plus3;
	}
	return plus2;
}
