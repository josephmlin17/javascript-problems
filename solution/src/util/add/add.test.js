import { add } from "./add";

test('Add returns function', () => {
	expect(add(0)).toBeInstanceOf(Function);
	expect(add(0)(1)).toBeInstanceOf(Function);
	expect(add(0)(1)(2)).toEqual(3);
	expect(add(100.1)(10.01)(1.001)).toEqual(111.111);
});

/**
 * These could be avoided via typescript
 */
test('Add Error tests', () => {
	expect(() => add('abc')).toThrow();
	expect(() => add(0)('abc')).toThrow();
	expect(() => add(0)(1)('abc')).toThrow();
	expect(() => add(true)).toThrow();
	expect(() => add({})).toThrow();
	expect(() => add(null)).toThrow();
	expect(() => add(() => { })).toThrow();
	expect(() => add(undefined)).toThrow();

});