/**
 * Iterates through the arrays and merges each one based on unique fields
 * @param  {...number[]} arrays the parameters are multiple arrays
 * @returns one array with all of the unique values 
 */
export const uniqueArray = (...arrays) => {
	if (arrays.length === 0) {
		throw 'No arrays entered';
	}
	let returnValue = [];
	for (let array of arrays) {
		returnValue = getUniqueArray(array, returnValue);
	}
	return returnValue;
}

/**
 * Iterates through the arrays via recursive means and merges each one based on unique fields
 * @param  {...any} arrays the parameters are multiple arrays
 * @returns one array with all of the unique values
 */
export const firstRecursiveCall = (...arrays) => {
	if (arrays.length === 0) {
		throw 'No Arrays entered';
	}
	arrays.unshift([]); // We need to add an array in the beginning to verify the type of arrays[0]
	return recursiveUnique(arrays);
}

const recursiveUnique = (arrays) => {
	if (arrays.length === 1) {
		return arrays[ 0 ];
	} else {
		const concatArray = getUniqueArray(arrays[ 0 ], arrays[ 1 ]);
		arrays.splice(0, 2);
		return recursiveUnique([ concatArray, ...arrays ]);
	}
}


/**
 * This is the main juice for combining two arrays. It holds the majority of the type checking and the finding the symmetric difference
 * @param {[number]} arrayA number array
 * @param {[number]} arrayB number array
 * @returns one array of unique values between the two arrays
 */
const getUniqueArray = (arrayA, arrayB) => {
	if (typeof arrayA !== typeof [] || typeof arrayB !== typeof []) {
		throw 'Invalid Object type';
	}
	// All values in B not in A
	let returnValue = arrayB.filter(value => {
		if (!Number.isInteger(value)) {
			throw 'Invalid Value in Array';
		}
		return arrayA.indexOf(value) === -1;
	});
	//All values in A not in B
	return returnValue.concat(arrayA.filter(value => {
		if (!Number.isInteger(value)) {
			throw 'Invalid value in Array';
		}
		return arrayB.indexOf(value) === -1;
	}));
}