import { uniqueArray, firstRecursiveCall } from './uniqueArray';
describe('Unique Array Function Test', () => {
	test('Test Unique Array with Valid Inputs', () => {
		// This tests varying input lengths
		expect(uniqueArray([ 1, 2, 3 ], [ 1, 2 ], [ 3, 4, 5 ], [ 6, 1 ])).toEqual([ 6, 1, 4, 5 ]);
		expect(uniqueArray([ 1, 2, 3, 4 ], [ 1, 2 ])).toEqual([ 3, 4 ]);
	});
	test('Test Unique Array with invalid inputs', () => {
		expect(() => uniqueArray([ 1, 2, 'b' ], [ 2, 3 ])).toThrow();
		expect(() => uniqueArray([ 1, 2, 3 ], { a: 'bcd' })).toThrow();
		expect(() => uniqueArray([ true ], [ 123, 456 ])).toThrow();
		expect(() => uniqueArray(true, [ 123, 456 ])).toThrow();
		expect(() => uniqueArray()).toThrow();
	});
});

describe('Unique Array via Recursive Function Test', () => {
	test('Test Unique Array via Recursive with Valid Inputs', () => {
		// This tests varying input lengths
		expect(firstRecursiveCall([ 1, 2, 3 ], [ 1, 2 ], [ 3, 4, 5 ], [ 6, 1 ])).toEqual([ 6, 1, 4, 5 ]);
		expect(firstRecursiveCall([ 1, 2, 3, 4 ], [ 1, 2 ])).toEqual([ 3, 4 ]);
	});
	test('Test Unique Array via Recursive with invalid inputs', () => {
		expect(() => firstRecursiveCall([ 1, 2, 'b' ], [ 2, 3 ])).toThrow();
		expect(() => firstRecursiveCall([ 1, 2, 3 ], { a: 'bcd' })).toThrow();
		expect(() => firstRecursiveCall([ true ], [ 123, 456 ])).toThrow();
		expect(() => firstRecursiveCall(true, [ 123, 456 ])).toThrow();
		expect(() => uniqueArray()).toThrow();
	});
})