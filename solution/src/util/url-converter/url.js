const reducer = (accumulator, currentValue, currentIndex) => {
	if (currentIndex === 0) {
		return accumulator + currentValue;
	}
	return accumulator += (`-${currentValue}`);
}

/**
 * Creates a url form of the string passed in
 * @param {string} unconvertedUrlString string that will be converted to url slug
 * @returns lowercase and hyphenated version of string  
 */
export const url = (unconvertedUrlString) => {
	if (typeof unconvertedUrlString !== 'string') {
		throw 'Invalid String type';
	}
	let subStrings = unconvertedUrlString.toLowerCase().split(' ');
	let urlString = '';
	return subStrings.reduce(reducer, urlString);
};