import { url } from './url';
describe('Test Suite', () => {

	/**
	 * Spaces should now be hyphens
	 */
	test('Url string converter', () => {
		expect(url('ABCDEFG')).toEqual('abcdefg');
		expect(url('ABC DEF GHI')).toEqual('abc-def-ghi');
		expect(url('')).toEqual('');
	});

	/**
	 * These could be avoided via typescript
	 */
	test('Url String Errors', () => {
		expect(() => url(123)).toThrow('Invalid String type');
		expect(() => url({})).toThrow();
		expect(() => url(null)).toThrow();
		expect(() => url(undefined)).toThrow();
		expect(() => url(true)).toThrow();
	});

})